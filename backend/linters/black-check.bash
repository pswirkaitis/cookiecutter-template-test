#!/bin/bash

backend_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)

black $@ $backend_dir --check
