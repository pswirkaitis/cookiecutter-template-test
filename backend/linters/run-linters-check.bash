#!/usr/bin/env bash

linters_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

$linters_dir/flake8.bash
$linters_dir/black-check.bash
$linters_dir/isort-check.bash
