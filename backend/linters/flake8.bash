#!/usr/bin/env bash

backend_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")/../" && pwd)

flake8 --config="$backend_dir/.flake8" "$backend_dir"
