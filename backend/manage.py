#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import inspect
import os
import sys


import dotenv
from django.conf import settings  # noqa


def if_exists_load_env(name: str) -> None:
    current_frame = inspect.currentframe()
    if not current_frame:
        return

    inspect_file = inspect.getfile(current_frame)
    env_path = os.path.dirname(os.path.abspath(inspect_file))
    env_file = "{env_path}/{name}".format(env_path=env_path, name=name)

    if os.path.exists(env_file):
        dotenv.load_dotenv(env_file, override=True)


def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_app.settings.dev')
    if_exists_load_env(".envs.local")
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
