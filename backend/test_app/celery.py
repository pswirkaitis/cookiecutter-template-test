import os
import sys

from celery import Celery
from django.conf import settings

settings_mode = os.environ.get("SETTINGS_MODE", "dev")
environment = os.environ.get("ENV_NAME", None)
if environment is None:
    settings_mode = settings_mode or "dev"
    if "dumpdata" not in sys.argv:
        print("Running celery in local environment on settings [{}]".format(settings_mode))
elif settings_mode is None:
    raise ImportError("NO SETTINGS SET - {}".format(environment))
else:
    print("Running celery on settings {}".format(settings_mode))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "test_app.settings.{}".format(settings_mode))

app = Celery("test_app")

app.config_from_object("django.conf:settings")
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.update(
    worker_max_memory_per_child=5000,
    result_backend=settings.CELERY_RESULT_BACKEND,
    broker_url=settings.CELERY_BROKER_URL,
    worker_send_task_events=True,
    beat_schedule={},
)
