"""
Write local settings here, or override base settings
"""
from test_app.settings._logging import generate_logging
from test_app.settings.base import *  # NOQA
from utils.settings import get_env_bool

CORS_ALLOWED_ORIGINS = [
    "http://localhost:3000",
    "http://127.0.0.1:3000",
]

DEBUG = True

BASE_SITE_URL = f"http://{APP_URL}"

TEMPLATES[0]["OPTIONS"]["debug"] = DEBUG  # type: ignore[index]

# Allow weak local passwords
AUTH_PASSWORD_VALIDATORS = []

INTERNAL_IPS = get_env("INTERNAL_IPS", default="").split(",")

# Add django debug toolbar when using local version
if get_env_bool("DEBUG_TOOLBAR", default=False):
    DEBUG_TOOLBAR_PATCH_SETTINGS = False

    INSTALLED_APPS += ["debug_toolbar"]
    MIDDLEWARE += ["debug_toolbar.middleware.DebugToolbarMiddleware"]
    DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": "test_app.settings.dev.show_toolbar"}

ENABLE_MAIL_SERVICE = get_env_bool("ENABLE_MAIL_SERVICE", False)
if not ENABLE_MAIL_SERVICE:
    EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"


# Allow django-debug-bar under docker
def show_toolbar(request):
    return True


DEBUG_SILK = get_env_bool("DEBUG_SILK", default=False)
if DEBUG_SILK:
    print("WARNING - SILK is enabled, backend may work much slower!")
    INSTALLED_APPS.append("silk")
    MIDDLEWARE.insert(0, "silk.middleware.SilkyMiddleware")

ENABLE_DEV_CACHE = get_env_bool("ENABLE_DEV_CACHE", default=False)
if ENABLE_DEV_CACHE:
    CACHES = {
        "default": {
            "BACKEND": "django_redis.cache.RedisCache",
            "LOCATION": f"redis://{REDIS_HOST}:6379/2",
            "OPTIONS": {
                "CLIENT_CLASS": "django_redis.client.DefaultClient",
            },
            "TIMEOUT": 900,
        },
    }

# Loggers
LOG_DIRECTORY = get_env("LOGS_PATH", os.path.join(PROJECT_ROOT, "logs"))
LOGGING = generate_logging(LOG_DIRECTORY, use_cloudwatch=False, disable_local_logs=False, debug_sql=False)
