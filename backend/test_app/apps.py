# -*- coding: utf-8 -*-
import os

from django.apps import AppConfig


class DefaultAppConfig(AppConfig):
    name = "test_app"

    def ready(self):
        """
        Fix for the EB health check host header:
        https://aalvarez.me/posts/setting-up-elastic-beanstalk-health-checks-with-a-django-application/
        https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-instance-metadata.html
        """
        import requests
        from django.conf import settings

        def get_ec2_instance_ip():
            raise NotImplementedError("The URI needs to be adjusted first")
            try:
                metadata_uri = os.environ.get("ECS_CONTAINER_METADATA_URI_V4", None)
                if metadata_uri is None:
                    return None
                container_metadata = requests.get(metadata_uri).json()
            except requests.exceptions.ConnectionError as e:
                return None
            return [ip for network in container_metadata["Networks"] for ip in network["IPv4Addresses"]]

        public_ips = get_ec2_instance_ip()
        if public_ips:
            settings.ALLOWED_HOSTS += public_ips


