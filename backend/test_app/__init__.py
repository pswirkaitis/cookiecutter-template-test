
from .celery import app as celery_app
from .signals import *

default_app_config = "test_app.apps.DefaultAppConfig"

__all__ = ("celery_app",)
