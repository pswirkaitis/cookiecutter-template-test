from django.utils.translation import ugettext_lazy as _
from og_django_utils.utils.named_enum import En, NamedEnum



class ValueEnum(NamedEnum):
    @classmethod
    def _values(cls):
        return {}

    @classmethod
    def value(cls, value, default=None):
        return cls._values().get(value, default)

    @classmethod
    def from_value(cls, search_value):
        for name, value in cls._values().items():
            if value == search_value:
                return name
        return None

    @classmethod
    def label_from_value(cls, search_value):
        value = cls.from_value(search_value)
        if value is None:
            return value
        return cls.name(value)

    @classmethod
    def labels_from_mask(cls, mask):
        values = []
        for value, flag in cls._values().items():
            if mask & flag or mask == flag == 0:
                values.append(value)
        return ", ".join(str(cls.name(value)) for value in values)


class TargetAWSRoles(NamedEnum):
    PRODUCTION = En(_("Production role"))
    STAGING = En(_("Staging role"))
class AsyncTaskStatus(NamedEnum):
    NEW = En(_("New"))
    PENDING = En(_("Pending"))
    PROCESSING = En(_("Processing"))
    COMPLETED = En(_("Completed"))
    ERROR = En(_("Error"))
    STEP_COMPLETED = En(_("Step completed"))
