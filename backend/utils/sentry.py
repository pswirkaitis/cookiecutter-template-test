# -*- coding: utf-8 -*-
import sentry_sdk


def capture_exception_with_scope(exception, tags, extras):
    with sentry_sdk.push_scope() as scope:
        for tag_name, tag_value in tags.items():
            scope.set_tag(tag_name, tag_value)
        for extra_name, extra_value in extras.items():
            scope.set_extra(extra_name, extra_value)
        sentry_sdk.capture_exception(exception)


def capture_message_with_scope(message, tags=None, extras=None):
    tags = tags or {}
    extras = extras or {}
    with sentry_sdk.push_scope() as scope:
        for tag_name, tag_value in tags.items():
            scope.set_tag(tag_name, tag_value)
        for extra_name, extra_value in extras.items():
            scope.set_extra(extra_name, extra_value)
        sentry_sdk.capture_message(message)
